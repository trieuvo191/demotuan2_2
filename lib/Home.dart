import 'package:flutter/material.dart';
import 'package:test1/Current.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'Home.dart';
import 'shop.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      

      title: 'Flutter Demo',
      theme: ThemeData(
       
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});


  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int currentIndex=0;
  final screens=[
      NavBar(),
      Current(),
      Categories(),
    ];
  @override
  Widget build (BuildContext context)=>Scaffold(
      body: screens[currentIndex],
      bottomNavigationBar:BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: currentIndex,
        onTap: (index)=>setState(() => currentIndex=index,),
        items: [
        
        BottomNavigationBarItem(icon: Icon(Icons.home),
        label: 'Home',
        ),
        BottomNavigationBarItem(icon: Icon(Icons.square_outlined),
        backgroundColor: Colors.blue,
        label: 'Orders'),
        BottomNavigationBarItem(icon: Icon(Icons.add_shopping_cart),
        backgroundColor: Colors.blue,
        label: 'Shop OTC'),
        BottomNavigationBarItem(icon: Icon(Icons.message),
        backgroundColor: Colors.blue,
        label: 'Message'),
        BottomNavigationBarItem(icon: Icon(Icons.notifications),
        label: 'Notifications'),
        
      ],
      ),
      );
    
  
      
      
  }
  
