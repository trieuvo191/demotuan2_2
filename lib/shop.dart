

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test1/ByBundleKit.dart';
import 'package:test1/ByCategories.dart';


class Categories extends StatefulWidget {
  @override
  Categories({Key ?key}) : super(key: key);
  _Categories createState() => _Categories();
}
class _Categories extends State<Categories>{
  List<bool>isSelected=[true,false];
  String numberEddit = '';
  final numberEditingController = TextEditingController();
  final GlobalKey<ScaffoldState>_scaffoldKey1=GlobalKey<ScaffoldState>();
  
  @override
  Widget build (BuildContext context)=>DefaultTabController(length: 2,
   child: Scaffold(
    key: _scaffoldKey1,
   
    
      appBar: AppBar(title: Text('Categories',style: TextStyle(color: Color.fromRGBO(4, 25, 103, 1),
      ),
      ),
      leading: InkWell(child: 
      Column(
        children: [
        Padding(padding: EdgeInsets.only(top: 16),
       child: SvgPicture.asset('assets/b.svg'),
        ),
          
            Padding(padding: EdgeInsets.only(top:5),
            child:SvgPicture.asset('assets/b.svg'),
            ),
          
          Padding(padding: EdgeInsets.only(top:5),
            child:SvgPicture.asset('assets/b.svg'),
            )

        ],
      ),
      onTap: (){
         _scaffoldKey1.currentState?.openDrawer();
      },
      ),
      centerTitle: true,
      actions: <Widget> [
            IconButton(
              icon: Icon(Icons.shopping_bag,color: Color.fromRGBO(4, 25, 103, 1),),
              onPressed: () => {
              },
      ),
        ],
        backgroundColor: Colors.yellow,
        bottom: PreferredSize(
          preferredSize:const Size.fromHeight(50) ,
          child: Container(
            height: 60,
            color: Colors.white,
            child: TabBar(tabs: [
              Tab(child: Text('By Categories',style: TextStyle(color: Color.fromRGBO(4, 25, 103, 1),fontSize: 15),),),
              Tab(child: Text('By Bundle Kit',style: TextStyle(color: Color.fromRGBO(4, 25, 103, 1),fontSize: 15),),),
            ],
            ),
          ),
          ),
      ),
      drawer:  Drawer( 
        child: ListView(
          children: []),),
      body: TabBarView(children: [
        ByCategories(),
        ByBundleKit(),
      ],),
      ),
  );
}

MaterialStateProperty<Color>getColor(Color color,Color colorPressed)
{
  final getColor=(Set<MaterialState>states){
    if(states.contains(MaterialState.pressed)){
      return colorPressed;
    }
    else{
      return color;
    }
    
  };
  return MaterialStateProperty.resolveWith(getColor);
}
MaterialStateProperty<BorderSide>getBorder(Color color,Color colorPressed)
{
  final getBorder=(Set<MaterialState>states){
    if(states.contains(MaterialState.pressed)){
      return BorderSide(color: colorPressed,width:2);
    }
    else{
      return BorderSide(color: color,width:2);
    }
  };
  return MaterialStateProperty.resolveWith(getBorder);
}